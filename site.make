core = 7.x
api = 2

; views_schema
projects[views_schema][type] = "module"
projects[views_schema][download][type] = "git"
projects[views_schema][download][url] = "https://git.uwaterloo.ca/drupal-org/views_schema.git"
projects[views_schema][download][tag] = "7.x-1.1"

; uw_site_database
projects[uw_site_database][type] = "module"
projects[uw_site_database][download][type] = "git"
projects[uw_site_database][download][url] = "https://git.uwaterloo.ca/wcms/uw_site_database.git"
projects[uw_site_database][download][tag] = "7.x-1.5"

; uw_wcms_tools
projects[uw_wcms_tools][type] = "module"
projects[uw_wcms_tools][download][type] = "git"
projects[uw_wcms_tools][download][url] = "https://git.uwaterloo.ca/wcms/uw_wcms_tools.git"
projects[uw_wcms_tools][download][tag] = "7.x-1.19"
